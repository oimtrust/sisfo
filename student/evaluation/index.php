<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 25/08/17
 * Time: 11:31
 */
error_reporting(0);
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}


$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");

if (isset($_POST['btn_send_evaluation'])){
    $npm    = strip_tags($_POST['npm']);
    $q1     = $_POST['q1'];
    $q2     = $_POST['q2'];
    $q3     = $_POST['q3'];
    $q4     = $_POST['q4'];
    $q5     = $_POST['q5'];
    $q6     = $_POST['q6'];

    if ($npm == ''){
        $students->redirect($baseUrl.'index.php?page=student&action=login');
    }
    elseif (empty($q1)){
        $error[] = "Soal 1 Harus anda jawab dahulu!";
    }
    elseif (empty($q2)){
        $error[] = "Soal 2 Harus anda jawab dahulu!";
    }
    elseif (empty($q3)){
        $error[] = "Soal 3 Harus anda jawab dahulu!";
    }
    elseif (empty($q4)){
        $error[] = "Soal 4 Harus anda jawab dahulu!";
    }
    elseif (empty($q5)){
        $error[] = "Soal 5 Harus anda jawab dahulu!";
    }
    elseif (empty($q6)){
        $error[] = "Soal 6 Harus anda jawab dahulu!";
    }
    else{
        try{
            if ($students->send_evaluation($q1, $q2, $q3, $q4, $q5, $q6, $npm)) {

            }
            $students->redirect($baseUrl.'index.php?page=student&action=bill-printout');
        }
        catch (Exception $exception){
            $exception->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/student/menu.view.php';
include 'apps/views/student/evaluation/index.view.php';
include 'apps/views/layouts/footer.view.php';