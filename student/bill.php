<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 10:22
 */
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");

$bill1 = $students->execute("SELECT
  daftar.id_pendaftar,
  daftar.npm,
  daftar.total,
  maprak.mata_praktikum,
  maprak.semester,
  mhs.nama_mhs,
  prodi.nama_prodi
FROM
  tbl_pendaftar AS daftar
  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
  LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
  LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
WHERE daftar.npm='{$student_login}'");

$data = $bill1->fetch_object();

$bill2 = $students->execute("SELECT
  daftar.id_pendaftar,
  daftar.npm,
  daftar.total,
  maprak.mata_praktikum,
  maprak.semester,
  mhs.nama_mhs,
  prodi.nama_prodi
FROM
  tbl_pendaftar AS daftar
  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
  LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
  LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
WHERE daftar.npm='{$student_login}'");

//accounting of maprak price
$price = $students->execute("SELECT SUM(total) AS price FROM tbl_pendaftar WHERE npm='{$student_login}'");

$total = $price->fetch_object();

include 'apps/views/layouts/header.view.php';
include 'apps/views/student/menu.view.php';
include 'apps/views/student/bill/index.view.php';
include 'apps/views/layouts/footer.view.php';