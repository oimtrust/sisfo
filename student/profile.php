<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 28/08/17
 * Time: 22:18
 */

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}
error_reporting(E_ALL);
ini_set('display_errors', 'On');

$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");

$profil       = $connect->execute("SELECT mhs.nama_mhs, prodi.nama_prodi 
                FROM tbl_mahasiswa AS mhs LEFT JOIN tbl_prodi AS prodi 
                ON mhs.id_prodi = prodi.id_prodi WHERE npm = '{$student_login}'");

$data = $profil->fetch_object();

include 'apps/views/layouts/header.view.php';
include 'apps/views/student/menu.view.php';
include 'apps/views/student/profile/index.view.php';
include 'apps/views/layouts/footer.view.php';