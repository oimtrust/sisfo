<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 24/08/17
 * Time: 16:34
 */
session_start();
session_destroy();
$connect->redirect($baseUrl . 'index.php?page=student&action=login');
exit;