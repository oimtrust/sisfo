<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 14:08
 */

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}


session_start();

//if logged in
if (isset($_SESSION['npm'])){
    $connect->redirect($baseurl . "index.php?page=student&action=dashboard");
    exit;
}

//if not logged in
if (isset($_POST['btn_login_student'])){
    $npm        = strip_tags($_POST['npm']);
    $password   = strip_tags(md5($_POST['password']));

    if ($npm == ''){
        $error[] = "NPM masih kosong!";
    }
    elseif ($password == ''){
        $error[] = "Password masih kosong!";
    }
    else{
        //check admin
        $check      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$npm}'");

        if ($check->num_rows == 0){
            $error[] = "NPM yang anda inputkan tidak terdaftar!";
        }
        else{
            //saving user session
            $login  = $check->fetch_assoc();
            if ($password == $login['password']){
                $_SESSION['npm'] = $login['npm'];
                $connect->redirect($baseurl. "index.php?page=student&action=dashboard");
                exit;
            }
            else {
                $error[] = "Password anda salah";
            }
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/student/auth/login.view.php';
include 'apps/views/layouts/footer.view.php';