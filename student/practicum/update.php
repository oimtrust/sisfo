<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 24/08/17
 * Time: 22:04
 */
$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");

//get edit_id for edited
if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])){
    $id     = $_GET['edit_id'];
    $stmt   = $students->execute("SELECT
                                  daftar.id_pendaftar,
                                  daftar.npm,
                                  daftar.total,
                                  maprak.id_maprak,
                                  maprak.mata_praktikum,
                                  maprak.semester
                                FROM
                                  tbl_pendaftar AS daftar
                                  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
                                WHERE id_pendaftar='{$id}'");
    $data   = $stmt->fetch_object();
}
else{
    $students->redirect($baseUrl.'index.php?page=student&action=practicum&error');
}

if (isset($_POST['btn_update_practicum'])){
    $id      = strip_tags($_POST['id_pendaftar']);
    $npm     = strip_tags($_POST['npm']);
    $imaprak = $_POST['maprak'];
    $total   = strip_tags($_POST['total']);

    if ($id == ''){
        $students->redirect($baseUrl.'index.php?page=student&action=practicum&error');
    }
    elseif ($npm == ''){
        $error[] = "Terjadi kesalahan sistem. Silahkan anda keluar dan masuk lagi.";
    }
    elseif (empty($imaprak)){
        $error[] = "Anda belum memilih Mata Praktikum";
    }
    elseif ($total == ''){
        $error[] = "Biaya masih kosong. Pilih mata praktikum untuk menentukan biaya";
    }
    else{
        try{
            if ($students->practicum_update($npm, $imaprak, $total, $id)){

            }
            $students->redirect($baseUrl.'index.php?page=student&action=practicum&updated');
        }
        catch (Exception $exception){
            $exception->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/student/menu.view.php';
include 'apps/views/student/practicum/update.view.php';
include 'apps/views/layouts/footer.view.php';