<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 9:33
 */
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");


include 'apps/views/layouts/header.view.php';
include 'apps/views/student/menu.view.php';
include 'apps/views/student/practicum/index.view.php';
include 'apps/views/layouts/footer.view.php';