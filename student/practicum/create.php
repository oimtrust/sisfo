<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 24/08/17
 * Time: 13:42
 */

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");

if (isset($_POST['btn_save_practicum'])){
    $npm        = strip_tags($_POST['npm']);
    $imaprak    = $_POST['maprak'];
    $total      = $_POST['total'];

    if ($npm == ''){
        $error[] = "Terjadi kesalahan sistem. Coba anda keluar dan masuk lagi";
    }
    elseif (empty($imaprak)){
        $error[] = "Mata praktikum harus dipilih";
    }
    elseif ($total == ''){
        $error[] = "Biaya masih kosong";
    }
    else {
        try{
            if ($students->practicum_create($npm, $imaprak, $total)){

            }
            $students->redirect($baseUrl.'index.php?page=student&action=practicum-create&saved');
        }
        catch (Exception $exception){
            $exception->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/student/menu.view.php';
include 'apps/views/student/practicum/create.view.php';
include 'apps/views/layouts/footer.view.php';