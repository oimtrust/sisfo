<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 28/08/17
 * Time: 20:30
 */
error_reporting(E_ALL);
ini_set('display_errors', 'On');

$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");

ob_start();

$now        = date('Y-m-d');
$filename   = 'Data Tagihan'.$student_login.'.pdf';

$query1  = $connect->execute("SELECT
                                      daftar.id_pendaftar,
                                      daftar.npm,
                                      daftar.total,
                                      maprak.mata_praktikum,
                                      maprak.semester,
                                      mhs.nama_mhs,
                                      prodi.nama_prodi
                                    FROM
                                      tbl_pendaftar AS daftar
                                      LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
                                      LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
                                      LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
                                    WHERE daftar.npm='{$student_login}'");

$row = $query1->fetch_object();

$content    = ob_get_clean();
$content    = "<table  style='border-bottom: 1px solid #999999; padding-bottom: 10px; width: 203mm;'>
                    <tr valign='top'>
                        <td style='width: 203mm;' valign='middle'>
                            <div style='font-weight: bold; padding-bottom: 5px; font-size: 12pt;'>
								Data Tagihan Mahasiswa
							</div>
							<span style='font-size: 10pt;'>Fakultas Teknologi Informasi</span>
                        </td>
                    </tr>
                </table>
                <p style='width: 210mm; font-size: 11pt;'>
                    <span style='font-size: 10pt;'>Nama     : $row->nama_mhs</span>
                    <span style='font-size: 10pt;'>Prodi    : $row->nama_prodi</span>
                    <span style='font-size: 10pt;'>Semester : $row->semester</span>
                </p>
                <table  cellpadding='0' cellspacing='1' style='width: 210mm;'>
                    <tr bgcolor='#CCCCCC'>
                        <th style='width: 15mm;'>No.</th> <!-- style='width: 10mm;' -->
                        <th style='width: 138mm;'>Mata Praktikum</th> <!--style='width: 50mm;'-->
                        <th style='width: 50mm;'>Biaya</th> <!--style='width: 30mm;' -->
                    </tr>";

$query2  = $connect->execute("SELECT
                                      daftar.id_pendaftar,
                                      daftar.npm,
                                      daftar.total,
                                      maprak.mata_praktikum,
                                      maprak.semester,
                                      mhs.nama_mhs,
                                      prodi.nama_prodi
                                    FROM
                                      tbl_pendaftar AS daftar
                                      LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
                                      LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
                                      LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
                                    WHERE daftar.npm='{$student_login}'");


//accounting of maprak price
$price = $connect->execute("SELECT SUM(total) AS price FROM tbl_pendaftar WHERE npm='{$student_login}'");

$total = $price->fetch_object();

$no = 1;

while ($data = $query2->fetch_object()) {
    $content    .= "<tr bgcolor='#FFFFFF'>
                        <td>$no</td>
                        <td>$data->mata_praktikum</td>
                        <td>$data->total</td>
                    </tr>";
    $no++;
}
$content    .= "<tr>
                    <th colspan='2'>Total : </th>
                    <td style='color: red'>
                        $total->price
                    </td>
                </tr></table>";

$content    .= "<table cellpadding='0' cellspacing='1' style='width: 210mm;'>
                 <tr>
                    <th style='width: 7mm'></th>
                    <th style='width: 150mm;'>Admin Sisfo,</th>
                    <th style='width: 130mm;'>Malang, __$now</th>
                 </tr>
                 <tr>
                    <td style='width: 7mm'></td>
                    <td style='width: 150mm;'></td>
                    <td style='width: 136mm;'>Mahasiswa</td>
                 </tr>
                 <tr>
                    <td style='width: 7mm'></td>
                    <td style='width: 150mm;'>
                    <br/>
                    <br/>
                    Shofi
                    </td>
                    <td style='width: 136mm;'>
                      <br/>
                      <br/>
                      <br/>
                      <br/>
                      $row->nama_mhs
                    </td>
                 </tr>
                </table>";
ob_end_clean();

//convertion html to pdf
try{
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->output($filename);
}
catch (\Spipu\Html2Pdf\Exception\Html2PdfException $exception){
    $exception->getMessage();
}
?>