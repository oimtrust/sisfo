<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 28/08/17
 * Time: 23:02
 */

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}
error_reporting(E_ALL);
ini_set('display_errors', 'On');

$student_login    = "";

//if not logged in
if (!isset($_SESSION['npm'])){
    $connect->redirect($baseUrl."index.php?page=student&action=login");
    exit;
}

//if logged in
$student_login = "{$_SESSION['npm']}";

//to retrive user data
$student      = $connect->execute("SELECT * FROM tbl_mahasiswa WHERE npm = '{$student_login}'");


if (isset($_POST['btn_update_password'])){
    $npm         = strip_tags($_POST['npm']);
    $old_pass    = strip_tags(md5($_POST['old_password']));
    $password    = strip_tags(md5($_POST['new_password']));

    $check = $students->execute("SELECT password FROM tbl_mahasiswa WHERE npm='{$student_login}'");
    $data  = $check->fetch_object();

    if ($npm == ''){
        $students->redirect($baseUrl.'index.php?page=student&action=login');
    }
    elseif ($old_pass != $data->password){
        $error[] = "Password lama tidak cocok";
    }
    elseif ($password == ''){
        $error[] = "Password baru tidak boleh kosong!";
    }
    elseif ($old_pass == ''){
        $error[] = "Password lama tidak boleh kosong!";
    }
    else {
        try {
            if ($students->update_password($npm, $password)){

            }
            $students->redirect($baseUrl.'index.php?page=student&action=profile-update&saved');
        }
        catch (Exception $exception){
            $exception->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/student/menu.view.php';
include 'apps/views/student/profile/update.view.php';
include 'apps/views/layouts/footer.view.php';