<?php
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_POST['btn_save_information'])) {
	$title 		= strip_tags($_POST['title']);
	$content 	= $_POST['content'];
	$created_at	= date('Y-m-d H:i:s');
	$updated_at	= date('Y-m-d H:i:s');

	if ($title == "") {
		$error[]	= "Judul pengumuman tidak boleh kosong!";
	}
	elseif ($content == "") {
		$error[]	= "Konten pengumuman harus ada dan wajib broo!";
	}
	else {
		try {
			if ($admins->information_create($title, $content, $created_at, $updated_at)) {
				
			}
			$admins->redirect($baseUrl. "index.php?page=admin&action=information-create&saved");
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/information/create.view.php';
include 'apps/views/layouts/footer.view.php';