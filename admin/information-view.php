<?php 
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

//get edit_id for edited
if (isset($_GET['view_id']) && !empty($_GET['view_id'])){
    $id_information = $_GET['view_id'];
    $stmt   		= $admins->execute("SELECT * FROM tbl_information WHERE id_information='{$id_information}'");
    $data   = $stmt->fetch_object();
}
else{
    $admins->redirect($baseUrl.'index.php?page=admin&action=information&error');
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/information/view.view.php';
include 'apps/views/layouts/footer.view.php';