<?php
error_reporting(0);

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_POST['btn_save'])) {
	$npm 		= strip_tags($_POST['npm']);
	$password	= strip_tags(md5($_POST['password']));
	$nama_mhs	= strip_tags($_POST['nama_mhs']);
	$id_prodi 	= $_POST['id_prodi'];
	$created_at = date('Y-m-d H:i:s');
	$updated_at = date('Y-m-d H:i:s');

	if ($npm == '') {
		$error[]	= "NPM masih kosong";
	}
	elseif ($password == '') {
		$error[]	= "Password masih kosong";
	}
	elseif ($nama_mhs == '') {
		$error[]	= "Nama Mahasiswa masih kosong";
	}
	elseif ($id_prodi == '') {
		$error[]	= "Progam Studi belum dipilih";
	}
	else {
		try {
			if ($admins->createParticipants($npm, $password, $nama_mhs, $id_prodi, $created_at, $updated_at)) {
				
			}
			$admins->redirect($baseUrl.'index.php?page=admin&action=participants-create&saved');
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/participants/create.view.php';
include 'apps/views/layouts/footer.view.php';