<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 9:06
 */

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

$evaluation = $connect->execute("SELECT 
	SUM(q1)/ count(id_evaluasi) AS q1, 
	SUM(q2)/ count(id_evaluasi) AS q2,
	SUM(q3)/ count(id_evaluasi) AS q3,
	SUM(q4)/ count(id_evaluasi) AS q4,
	SUM(q5)/ count(id_evaluasi) AS q5,
	SUM(q6)/ count(id_evaluasi) AS q6 FROM tbl_evaluasi");

$evaluation_detail = $connect->execute("SELECT 
	SUM(q1)/ count(id_evaluasi) AS q1, 
	SUM(q2)/ count(id_evaluasi) AS q2,
	SUM(q3)/ count(id_evaluasi) AS q3,
	SUM(q4)/ count(id_evaluasi) AS q4,
	SUM(q5)/ count(id_evaluasi) AS q5,
	SUM(q6)/ count(id_evaluasi) AS q6 FROM tbl_evaluasi");

$user_evaluation = $connect->execute("SELECT COUNT(*) AS total FROM tbl_evaluasi");

$echo_user_ev    = $user_evaluation->fetch_object();

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/evaluation/index.view.php';
include 'apps/views/layouts/footer.view.php';