<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 15:28
 */
session_start();
session_destroy();
$connect->redirect($baseUrl . 'index.php?page=admin&action=login');
exit;