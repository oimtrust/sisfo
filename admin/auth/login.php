<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 14:08
 */

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

session_start();

//if logged in
if (isset($_SESSION['username'])){
    $connect->redirect($baseurl . "index.php?page=admin&action=dashboard");
    exit;
}

//if not logged in
if (isset($_POST['btn_login'])){
    $username   = strip_tags($_POST['username']);
    $password   = strip_tags(md5($_POST['password']));

    if ($username == ''){
        $error[] = "Username masih kosong!";
    }
    elseif ($password == ''){
        $error[] = "Password masih kosong!";
    }
    else{
        //check admin
        $check      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$username}'");

        if ($check->num_rows == 0){
            $error[] = "Username yang anda inputkan tidak terdaftar!";
        }
        else{
            //saving user session
            $login  = $check->fetch_assoc();
            if ($password == $login['password']){
                $_SESSION['username'] = $login['username'];
                $connect->redirect($baseurl. "index.php?page=admin&action=dashboard");
                exit;
            }
            else {
                $error[] = "Password anda salah";
            }
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/auth/login.view.php';
include 'apps/views/layouts/footer.view.php';