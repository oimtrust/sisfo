<?php 
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

//get edit_id for edited
if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])){
    $id_information = $_GET['edit_id'];
    $stmt   		= $admins->execute("SELECT * FROM tbl_information WHERE id_information='{$id_information}'");
    $data   = $stmt->fetch_object();
}
else{
    $admins->redirect($baseUrl.'index.php?page=admin&action=information&error');
}

if (isset($_POST['btn_update_information'])) {
	$id_information = $_POST['id_information'];
	$title 			= $_POST['title'];
	$content		= $_POST['content'];
	$updated_at		= date('Y-m-d H:i:s');

	if ($title == '') {
		$error[]	= "Judul tidak boleh kosong";
	}
	elseif ($content == '') {
		$error[]	= "Konten tidak boleh kosong";
	}
	else {
		try {
			if ($admins->information_update($title, $content, $updated_at, $id_information)) {
				
			}
			$admins->redirect($baseUrl . "index.php?page=admin&action=information-edit&saved");
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/information/edit.view.php';
include 'apps/views/layouts/footer.view.php';