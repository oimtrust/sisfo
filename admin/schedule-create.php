<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 23:51
 */
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_POST['btn_save_schedule'])){
    $maprak     = strip_tags($_POST['maprak']);
    $semester   = $_POST['semester'];
    $prodi      = $_POST['prodi'];
    $harga      = strip_tags($_POST['harga']);

    if ($maprak == ''){
        $error[] = "Mata Praktikum harus di isi";
    }
    elseif ($semester == ''){
        $error[] = "Semester harus dipilih";
    }
    elseif ($prodi == ''){
        $error[] = "Prodi harus dipilih";
    }
    elseif ($harga == ''){
        $error[] = "Harga harus dimasukkan";
    }
    else{
        try{
            if ($admins->schedule_create($maprak, $semester, $prodi, $harga)){

            }
            $admins->redirect($baseUrl.'index.php?page=admin&action=schedule-create&saved');
        }
        catch (Exception $exception){
            $exception->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/schedule/create.view.php';
include 'apps/views/layouts/footer.view.php';