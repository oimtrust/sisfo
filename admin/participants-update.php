<?php

if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {
    $npm = $_GET['edit_id'];
    $stmt = $connect->execute("SELECT
                              mhs.npm,
                              mhs.nama_mhs,
                              mhs.password,
                              prodi.id_prodi,
                              prodi.nama_prodi
                            FROM
                              tbl_mahasiswa AS mhs
                              LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
                            WHERE mhs.npm = '{$npm}'");
    $data = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=admin&action=participants&error');
}

if (isset($_POST['btn_update'])) {
	$npm 		= strip_tags($_POST['npm']);
	$password	= strip_tags(md5($_POST['password']));
	$nama_mhs	= strip_tags($_POST['nama_mhs']);
	$id_prodi	= $_POST['id_prodi'];
	$updated_at	= date('Y-m-d H:i:s');

	if ($password == '') {
		$error[]	= "Password masih kosong";
	}
	elseif ($nama_mhs == '') {
		$error[]	= "Nama Mahasiswa masih kosong";
	}
	elseif ($id_prodi == '') {
		$error[]	= "Prodi belum dipilih";
	}
	else {
		try {
			if ($admins->updateParticipants($password, $nama_mhs, $id_prodi, $updated_at, $npm)) {
				
			}
			$admins->redirect($baseUrl.'index.php?page=admin&action=participants&updated');
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/participants/update.view.php';
include 'apps/views/layouts/footer.view.php';