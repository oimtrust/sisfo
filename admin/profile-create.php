<?php 
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_POST['btn_save'])) {
	$username 		= strip_tags($_POST['username']);
	$password 		= strip_tags(md5($_POST['password']));
	$created_at		= date('Y-m-d H:i:s');
	$updated_at		= date('Y-m-d H:i:s');

	if ($username == '') {
		$error[]	= "Username masih kosong!";
	}
	elseif ($password == '') {
		$error[]	= "Password masih kosong!";
	}
	else {
		try {
			if ($admins->createProfile($username, $password, $created_at, $updated_at)) {
				
			}
			$admins->redirect($baseUrl.'index.php?page=admin&action=profile-create&saved');
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/profile/create.view.php';
include 'apps/views/layouts/footer.view.php';