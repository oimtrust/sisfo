<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 04/09/17
 * Time: 21:46
 */

//this function for sending raw excel data
header("Content-type: application/vnd-ms-excel");

$now        = date('Y-m-d');

//definition name of file
header("Content-Disposition: attachment; filename=data-praktikan-'{$now}'.xls");
?>

<table border="1">
    <tr>
        <th>No.</th>
        <th>NPM</th>
        <th>Nama Mahasiswa</th>
        <th>Mata Praktikum</th>
        <th>Semester</th>
        <th>Progam Studi</th>
    </tr>

    <?php
    $query = $connect->execute("SELECT daftar.npm,  GROUP_CONCAT(maprak.mata_praktikum SEPARATOR ', ') as maprak, maprak.semester, mhs.nama_mhs, prodi.nama_prodi
 FROM
tbl_pendaftar AS daftar
 LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
 LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
 LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
GROUP BY daftar.npm");
    $no     = 1;

    while ($data = $query->fetch_object()){
        ?>
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $data->npm;?></td>
            <td><?php echo $data->nama_mhs;?></td>
            <td><?php echo $data->maprak;?></td>
            <td><?php echo $data->semester;?></td>
            <td><?php echo $data->nama_prodi;?></td>
        </tr>
        <?php
        $no++;
    }
    ?>
</table>
