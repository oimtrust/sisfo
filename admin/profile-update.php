<?php
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {
    $id_admin = $_GET['edit_id'];
    $stmt = $connect->execute("SELECT * FROM tbl_admin WHERE id_admin = '{$id_admin}'");
    $data = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=admin&action=profile&error');
}

if (isset($_POST['btn_update'])) {
	$id_admin 	= $_POST['id_admin'];
	$username 	= strip_tags($_POST['username']);
	$password 	= strip_tags(md5($_POST['password']));
	$updated_at = date('Y-m-d H:i:s');

	if ($username == '') {
		$error[]	= "Username masih kosong!";
	}
	elseif ($password == '') {
		$error[]	= "Password masih kosong!";
	}
	else {
		try {
			if ($admins->updateProfile($username, $password, $updated_at, $id_admin)) {
				
			}
			$admins->redirect($baseUrl.'index.php?page=admin&action=profile&updated');
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}


include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/profile/update.view.php';
include 'apps/views/layouts/footer.view.php';