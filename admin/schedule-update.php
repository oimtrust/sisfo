<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 9:00
 */
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$admin_login    = "";

//if not logged in
if (!isset($_SESSION['username'])){
    $connect->redirect($baseUrl."index.php?page=admin&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin      = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

//get edit_id for edited
if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])){
    $id     = $_GET['edit_id'];
    $stmt   = $admins->execute("SELECT id_maprak, mata_praktikum, harga FROM tbl_maprak WHERE id_maprak='{$id}'");
    $data   = $stmt->fetch_object();
}
else{
    $admins->redirect($baseUrl.'index.php?page=admin&action=schedule&error');
}

if (isset($_POST['btn_update_schedule'])){
    $id         = $_POST['id_maprak'];
    $maprak     = strip_tags($_POST['maprak']);
    $semester   = $_POST['semester'];
    $prodi      = $_POST['prodi'];
    $harga      = strip_tags($_POST['harga']);

    if ($id == ''){
        $admins->redirect($baseUrl.'index.php?page=admin&action=schedule&error');
    }
    elseif ($maprak == ''){
        $error[]    = "Mata praktikum harus di isi";
    }
    elseif ($semester == ''){
        $error[]    = "Semester harus dipilih";
    }
    elseif ($prodi == ''){
        $error[]    = "Prodi harus dipilih";
    }
    elseif ($harga == ''){
        $error[]    = "Harga harus di isi";
    }
    else{
        try{
            if ($admins->schedule_update($maprak, $semester, $prodi, $harga, $id)){

            }
            $admins->redirect($baseUrl.'index.php?page=admin&action=schedule&saved');
        }
        catch (Exception $exception){
            $exception->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/admin/menu.view.php';
include 'apps/views/admin/schedule/update.view.php';
include 'apps/views/layouts/footer.view.php';