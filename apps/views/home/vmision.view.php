<div class="container">
    <div class="mx-auto" style="height: 99px;"></div>
        <ul class="list-unstyled">
            <li class="media">
                <div class="media-body">
                <h5 class="mt-0 mb-1">VISI</h5>
                Menjadi Fakultas Unggul dalam Menyelenggarakan Pendidikan yang 
                Mengintegrasikan Sains dan Teknologi Informasi Pada Tahun 2025
                </div>
            </li>
            <li class="media my-4">
                <div class="media-body">
                <h5 class="mt-0 mb-1">MISI</h5>
                <ol>
                    <li>
                    Berdaya saing tinggi, dengan mengembangkan pelaksanaan pembelajaran berbasis sains 
                    dan teknologi informasi dengan mengikuti kemajuan Iptek
                    </li>
                    <li>
                    Penguasaan Iptek, dengan mengembangkan pelaksanaan 
                    aktifitas penelitian di bidang sains dan teknologi informasi
                    </li>
                    <li>
                    Berkebudayaan nasional, dengan melaksanakan pengabdian kepada masyarakat di bidang 
                    sains dan teknologi informasi guna mencerdaskan kehidupan masyarakat.
                    </li>
                    <li>
                    Berwawasan global,
                    dengan memperkuat jalinan kerjasama di bidang sains dan teknologi informasi dengan berbagai pihak
                    </li>
                </ol>
                </div>
            </li>
        </ul>
    <div class="mx-auto" style="height: 99px;"></div>
</div>