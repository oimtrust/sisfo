<div class="container">
    <div class="mx-auto" style="height: 99px;"></div>
    <!-- Start Body -->

    <?php 
    $query = $admins->execute("SELECT id_information, title, content, created_at, updated_at FROM tbl_information ORDER BY updated_at DESC");

    while ($row_data = $query->fetch_object()) {
      ?>
      <div class="row">
          <div class="col-sm-6">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title"><?php echo $row_data->title; ?></h4>
                <p class="card-text">
                  <?php echo $row_data->content; ?>
                </p>
              </div>
            </div>
        </div>
      </div>
      <?php
    }
     ?>
    <!-- End Body -->
    <div class="mx-auto" style="height: 99px;"></div>
</div>