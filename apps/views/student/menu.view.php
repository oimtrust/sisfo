<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 13:56
 */?>
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #e3f2fd;">
    <div class="container">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?php $baseUrl;?>index.php?page=student&action=dashboard">SISFO</a>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav nav justify-content-end">
            <li class="nav-item active">
                <a class="nav-link" href="<?php $baseUrl;?>index.php?page=student&action=dashboard">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php $baseUrl;?>index.php?page=student&action=practicum">Praktikum <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php $baseUrl;?>index.php?page=student&action=bill">Lihat Tagihan</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $student_login;?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=student&action=profile">Lihat Profil</a>
                    <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=student&action=logout">Keluar</a>
                </div>
            </li>
        </ul>
    </div>
    </div>
</nav>