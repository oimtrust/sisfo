<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 10:21
 */?>
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Data Tagihan Mahasiswa</h4>
            <p class="card-text">
            <h5>Nama :</h5><?php echo $data->nama_mhs;?>
            <h5>Prodi :</h5><?php echo $data->nama_prodi;?>
            <h5>Semester :</h5><?php echo $data->semester;?>
            </p>
            <a href="<?php $baseUrl;?>index.php?page=student&action=evaluation" class="btn btn-warning">Cetak</a>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table  table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Mata Praktikum</th>
                    <th>Biaya</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                while ($row = $bill2->fetch_object()){
                    ?>
                    <tr class="<?php if($no % 2 == 0) {echo "odd";} else {echo "even";} ?>">
                        <th scope="row"><?php echo $no;?></th>
                        <td><?php echo $row->mata_praktikum;?></td>
                        <td><?php echo $row->total;?></td>
                    </tr>
                    <?php
                    $no++;
                }
                ?>
                <tr>
                    <th colspan="2">Total : </th>
                    <td>
                        <p class="text-danger">
                            <?php echo $total->price;?>
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
