<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 25/08/17
 * Time: 11:11
 */?>
<div class="container">
    <div class="card">
        <form method="post">
            <h3 class="card-header">Soal Evaluasi</h3>
            <div class="card-body">
                <?php
                        if (isset($error)){
                            foreach ($error as $error){
                                ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Peringatan!</strong> <?php echo $error;?>
                                </div>
                                <?php
                            }
                        }
                        elseif (isset($_GET['saved'])){
                            ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Selamat!</strong> Penyimpanan data berhasil.
                            </div>
                            <?php
                        }
                ?>
                <input type="hidden" name="npm" value="<?php echo $student_login;?>">

                <!--Question 1-->
                <section id="q1">
                    <h6 class="card-title">1. Waktu respon membuka alamat ini sampai
                        keluar halaman login sangat cepat.</h6>
                    <label class="custom-control custom-radio">
                        <input id="q1-1" name="q1" type="radio" value="100" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q1-2" name="q1" type="radio" value="80" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q1-3" name="q1" type="radio" value="60" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Ragu-Ragu</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q1-4" name="q1" type="radio" value="40" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Tidak Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q1-5" name="q1" type="radio" value="20" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Tidak Setuju</span>
                    </label>
                </section>

                <!--Question 2-->
                <section id="q2">
                    <h6 class="card-title">2. Informasi dalam sistem ini sangat membantu
                        mahasiswa.</h6>
                    <label class="custom-control custom-radio">
                        <input id="q2-1" name="q2" type="radio" value="100" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q2-2" name="q2" type="radio" value="80" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q2-3" name="q2" type="radio" value="60" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Ragu-Ragu</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q2-4" name="q2" type="radio" value="40" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Tidak Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q2-5" name="q2" type="radio" value="20" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Tidak Setuju</span>
                    </label>
                </section>

                <!--Question 3-->
                <section id="q3">
                    <h6 class="card-title">3. Biaya yang dikeluarkan untuk memperbaharui
                        sistem ini tidak terlalu mahal.</h6>
                    <label class="custom-control custom-radio">
                        <input id="q3-1" name="q3" type="radio" value="100" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q3-2" name="q3" type="radio" value="80" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q3-3" name="q3" type="radio" value="60" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Ragu-Ragu</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q3-4" name="q3" type="radio" value="40" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Tidak Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q3-5" name="q3" type="radio" value="20" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Tidak Setuju</span>
                    </label>
                </section>

                <!--Question 4-->
                <section id="q4">
                    <h6 class="card-title">4. Kenyamanan mahasiswa dalam mengakses
                        web ini apa perlu di perbaiki.</h6>
                    <label class="custom-control custom-radio">
                        <input id="q4-1" name="q4" type="radio" value="100" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-2" name="q4" type="radio" value="80" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-3" name="q4" type="radio" value="60" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Ragu-Ragu</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-4" name="q4" type="radio" value="40" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Tidak Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-5" name="q4" type="radio" value="20" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Tidak Setuju</span>
                    </label>
                </section>

                <!--Question 5-->
                <section id="q5">
                    <h6 class="card-title">5. Efisien proses yang di lalukan dalam
                        melakukan registrasi tidak perlu di perbaiki.</h6>
                    <label class="custom-control custom-radio">
                        <input id="q4-1" name="q5" type="radio" value="100" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-2" name="q5" type="radio" value="80" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-3" name="q5" type="radio" value="60" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Ragu-Ragu</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-4" name="q5" type="radio" value="40" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Tidak Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-5" name="q5" type="radio" value="20" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Tidak Setuju</span>
                    </label>
                </section>

                <!--Question 6-->
                <section id="q6">
                    <h6 class="card-title">5. Pelayanan sistem ini sangat akurat, mudah
                        digunakan dan dapat dipercaya.</h6>
                    <label class="custom-control custom-radio">
                        <input id="q4-1" name="q6" type="radio" value="100" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-2" name="q6" type="radio" value="80" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-3" name="q6" type="radio" value="60" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Ragu-Ragu</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-4" name="q6" type="radio" value="40" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Tidak Setuju</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input id="q4-5" name="q6" type="radio" value="20" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Sangat Tidak Setuju</span>
                    </label>
                </section>
            </div>
            <div class="card-header">
                <button type="submit" name="btn_send_evaluation" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
</div>
