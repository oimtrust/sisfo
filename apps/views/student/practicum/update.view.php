<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 24/08/17
 * Time: 13:41
 */?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                    <fieldset>
                        <?php
                        if (isset($error)){
                            foreach ($error as $error){
                                ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Peringatan!</strong> <?php echo $error;?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <form method="post">
                            <fieldset >
                                <!--NPM hidden is here-->
                                <input type="hidden" name="npm" value="<?php echo $student_login;?>">
                                <input type="hidden" name="id_pendaftar" value="<?php echo $data->id_pendaftar;?>">
                                <div class="form-group">
                                    <?php
                                    $maprak       = $connect->execute("SELECT * FROM tbl_maprak WHERE id_maprak ORDER BY id_maprak ASC");
                                    $row_count    = $maprak->num_rows;
                                    ?>
                                    <label for="maprak">Mata Praktikum</label>
                                    <select id="maprak" name="maprak" class="form-control">
                                        <option value="<?php $data->id_maprak;?>" selected><?php echo $data->mata_praktikum;?></option>
                                        <?php
                                        if ($row_count > 0){
                                            while ($row = $maprak->fetch_object()){
                                                echo '<option value="'.$row->id_maprak.'">'.$row->mata_praktikum.'</option>';
                                            }
                                        }
                                        else{
                                            echo '<option value="">Praktikum tidak tersedia</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="semester">Semester</label>
                                    <input type="text" id="semester" name="semester" readonly value="<?php echo $data->semester;?>" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="price">Biaya</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        <input type="text" name="total" id="total" value="<?php echo $data->total;?>" readonly class="form-control">
                                    </div>
                                </div>
                                <button type="submit" name="btn_update_practicum" class="btn btn-info">Ubah</button>
                                <a href="<?php $baseUrl;?>index.php?page=student&action=practicum" class="btn btn-warning">Batal</a>
                            </fieldset>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    (function($){
        $(function(){

            //Get data from tbl_maprak
            $("#maprak").change(function () {
                var id = $(this).val();
                var dataString = 'id='+id;

                $.ajax({
                    type: "POST",
                    url: "<?php $baseUrl;?>index.php?page=student&action=practicum-create-dynamix",
                    data: dataString,
                    dataType: 'JSON',
                    cache: false,
                    success: function (responses) {
                        $('#total').val(responses.total)
                        $('#semester').val(responses.semester)
                        //console.log(responses.total);
                    }
                });
            });

            $(".only_number").keypress(function (data)
            {
                // if not number, showing error message
                if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57))
                {
//                    $("#message").html("Inputan tidak valid").show().fadeOut("slow");
                    return false;
                }
            });

        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>
