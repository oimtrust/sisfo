<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 9:27
 */?>

<div class="container">
    <div class="card">
        <div class="card-body">
            <h2>Mata Praktikum</h2>
            <a href="<?php $baseUrl;?>index.php?page=student&action=practicum-create" class="btn btn-primary">Daftar</a>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <?php
                if (isset($_GET['error'])){
                    ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Peringatan!</strong> Anda harus memilih data untuk di ubah.
                    </div>
                    <?php
                }
                elseif (isset($_GET['updated'])){
                            ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Selamat!</strong> Perubahan data berhasil.
                </div>
                <?php
                }
                elseif (isset($_GET['deleted'])){
                    ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Selamat!</strong> Data berhasil dihapus.
                    </div>
                    <?php
                }
                ?>
                <div class="table-responsive">
                    <table class="table  table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Mata Praktikum</th>
                            <th>Semester</th>
                            <th>Biaya</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $limit = 5;
                        $pagination = isset($_GET['pagination']) ? $_GET['pagination'] : "";

                        if (empty($pagination)){
                            $position   = 0;
                            $pagination = 1;
                        }
                        else {
                            $position   = ($pagination - 1) * $limit;
                        }

                        $sql = $students->execute("SELECT
                                  daftar.id_pendaftar,
                                  daftar.npm,
                                  daftar.total,
                                  maprak.mata_praktikum,
                                  maprak.semester
                                FROM
                                  tbl_pendaftar AS daftar
                                  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
                                WHERE daftar.npm=$student_login LIMIT $position, $limit");
                        $no = 1 + $position;

                        while ($data = $sql->fetch_object()){
                            ?>
                            <tr class="<?php if($no % 2 == 0) {echo "odd";} else {echo "even";} ?>">
                                <th scope="row"><?php echo $no;?></th>
                                <td ><?php echo $data->mata_praktikum;?></td>
                                <td ><?php echo $data->semester;?></td>
                                <td ><?php echo $data->total;?></td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group">
                                        <a href="<?php $baseUrl;?>index.php?page=student&action=practicum-update&edit_id=<?php echo $data->id_pendaftar;?>" class="btn btn-secondary btn-info">Ubah</a>
                                        <a href="<?php $baseUrl;?>index.php?page=student&action=practicum-delete&delete_id=<?php echo $data->id_pendaftar;?>" class="btn btn-secondary btn-danger btn-delete">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $no++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <nav>
                    <?php
                    $amount_data = $students->execute("SELECT
                                  daftar.id_pendaftar,
                                  daftar.npm,
                                  daftar.total,
                                  maprak.mata_praktikum,
                                  maprak.semester
                                FROM
                                  tbl_pendaftar AS daftar
                                  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
                                WHERE daftar.npm=$student_login");
                    $rows   = $amount_data->num_rows;

                    $amount_page = ceil($rows /$limit);

                    /**
                     * previous page
                     */
                    if ($pagination > 1){
                        $link = $pagination - 1;
                        $prev = "<a class='page-link' href='".$baseUrl."index.php?page=student&action=practicum&pagination=$link'>Previous</a>";
                    }
                    else {
                        $prev = "<a class='page-link' href='#'>Previous</a>";
                    }

                    /**
                     * navigation after page/next
                     */
                    if ($pagination < $amount_page){
                        $link = $pagination + 1;
                        $next = "<a class='page-link' href='".$baseUrl."index.php?page=student&action=practicum&pagination=$link'>Next</a>
                            ";
                    }
                    else{
                        $next = "<a class='page-link' href='#'>Next</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                    }
                    echo "<ul class='pagination justify-content-end'>
                        <li class='page-item'>
                            ".$prev."
                        </li>
                        <li class='page-item'>
                            ".$next."
                        </li>
                    </ul>"
                    ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<!-- Script JS -->
<script type="text/javascript">

    $('.btn-delete').on('click',function(){
        var getLink = $(this).attr('href');

        swal({
            title: 'Hapus Mata Praktikum',
            text: 'Anda Yakin?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        },function(){
            window.location.href = getLink
        });

        return false;
    });
</script>