<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 24/08/17
 * Time: 13:41
 */?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                    <fieldset>
                        <?php
                        if (isset($error)){
                            foreach ($error as $error){
                                ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Peringatan!</strong> <?php echo $error;?>
                                </div>
                                <?php
                            }
                        }elseif (isset($_GET['saved'])){
                            ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Selamat!</strong> Penyimpanan data berhasil. Lihat <a href="<?php $baseUrl;?>index.php?page=student&action=practicum">disini</a>
                            </div>
                            <?php
                        }
                        ?>
                        <form method="post">
                            <fieldset >
                                <!--NPM hidden is here-->
                                <input type="hidden" name="npm" value="<?php echo $student_login;?>">
                                <div class="form-group">
                                    <?php
                                    $maprak       = $connect->execute("SELECT * FROM tbl_maprak WHERE id_maprak ORDER BY id_maprak ASC");
                                    $row_count    = $maprak->num_rows;
                                    ?>
                                    <label for="maprak">Mata Praktikum</label>
                                    <select id="maprak" name="maprak" class="form-control">
                                        <option value="" selected>Pilih Mata Praktikum</option>
                                        <?php
                                        if ($row_count > 0){
                                            while ($row = $maprak->fetch_object()){
                                                echo '<option value="'.$row->id_maprak.'">'.$row->mata_praktikum.'</option>';
                                            }
                                        }
                                        else{
                                            echo '<option value="">Praktikum tidak tersedia</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="semester">Semester</label>
                                    <input type="text" id="semester" name="semester" readonly class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="price">Biaya</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        <input type="text" name="total" id="total" readonly class="form-control">
                                    </div>
                                </div>
                                <button type="submit" name="btn_save_practicum" class="btn btn-primary">Tambah</button>
                                <a href="<?php $baseUrl;?>index.php?page=student&action=practicum" class="btn btn-warning">Batal</a>
                            </fieldset>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    (function($){
        $(function(){

            //Get data from tbl_maprak
            $("#maprak").change(function () {
                var id = $(this).val();
                var dataString = 'id='+id;

                $.ajax({
                    type: "POST",
                    url: "<?php $baseUrl;?>index.php?page=student&action=practicum-create-dynamix",
                    data: dataString,
                    dataType: 'JSON',
                    cache: false,
                    success: function (responses) {
                        $('#total').val(responses.total)
                        $('#semester').val(responses.semester)
                        //console.log(responses.total);
                    }
                });
            });

            $(".only_number").keypress(function (data)
            {
                // if not number, showing error message
                if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57))
                {
//                    $("#message").html("Inputan tidak valid").show().fadeOut("slow");
                    return false;
                }
            });

        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>
