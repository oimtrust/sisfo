<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 11:29
 */?>
<!-- Navigation
    ================================================== -->
<div class="hero-background">
    <div>
        <img class="strips" src="<?php $baseUrl;?>public/assets/images/strips.png">
    </div>
    <div class="container">
        <div class="header-container header">
            <a class="navbar-item logo" href="#"> <img class="logo" src="<?php $baseUrl;?>public/assets/images/logosS.png"/> </a>
        </div>
        <!--navigation-->


        <!-- Hero-Section
          ================================================== -->

        <div class="hero row">
            <div class="hero-right col-sm-6 col-sm-6">
                <?php
                if (isset($error)){
                    foreach ($error as $error){
                        ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Peringatan!</strong> <?php echo $error;?>
                        </div>
                        <?php
                    }
                }
                ?>
                <form method="post">
                    <div class="form-group">
                        <label for="npm">NPM</label>
                        <input type="text" name="npm" class="form-control" id="npm" >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" >
                    </div>
                    <button type="submit" name="btn_login_student" class="btn btn-primary">Masuk</button>
                    <a href="<?php $baseUrl;?>index.php" class="btn btn-warning">Kembali</a>
                </form>
            </div><!--hero-left-->

        </div><!--hero-->

    </div> <!--hero-container-->

</div><!--hero-background-->

<script type="text/javascript">
    $(".alert").alert()
</script>