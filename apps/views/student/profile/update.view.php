<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 28/08/17
 * Time: 23:01
 */?>
<div class="container">
    <div class="card">
        <form method="post">
            <h3 class="card-header">Ubah Password</h3>
            <div class="card-block">
                <?php
                if (isset($error)){
                    foreach ($error as $error){
                        ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Peringatan!</strong> <?php echo $error;?>
                        </div>
                        <?php
                    }
                }
                elseif (isset($_GET['saved'])){
                    ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Selamat!</strong> Perubahan data berhasil.
                    </div>
                    <?php
                }
                ?>
                <input type="hidden" name="npm" value="<?php echo $student_login;?>">

                <div class="form-group">
                    <label for="old_password">Password Lama</label>
                    <input type="password" name="old_password" class="form-control" id="old_password">
                </div>
                <div class="form-group">
                    <label for="new_password">Password Baru</label>
                    <input type="password" name="new_password" class="form-control" id="new_password">
                </div>
            </div>
            <div class="card-header">
                <button type="submit" name="btn_update_password" class="btn btn-primary">Ubah</button>
            </div>
        </form>
    </div>
</div>
