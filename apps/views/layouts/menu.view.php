
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #e3f2fd;">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
            <a class="navbar-brand" href="<?php $baseUrl;?>index.php">SISFO</a>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php $baseUrl;?>index.php">Beranda <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php $baseUrl;?>index.php?page=home&action=vmision">Visi & Misi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php $baseUrl;?>index.php?page=home&action=information">Informasi Mahasiswa</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Login
                        </a>
                        <div class="dropdown-menu" >
                            <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=admin&action=login">Admin</a>
                            <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=student&action=login">Mahasiswa</a>
                        </div>
                    </li>
                </ul>
            </div>
            </div>
        </nav>
