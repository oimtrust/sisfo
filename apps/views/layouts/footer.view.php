<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 11:29
 */?>

<!-- Footer
  ================================================== -->

<div class="footer">

    <div class="container">
        <div class="row">

            <div class="col-sm-2"></div>

            <div class="col-sm-8 webscope">
                <span class="webscope-text"> Designed by </span>
                <a href="http://oimtrust.com/kopi/" target="_blank"> KOPI</a>
            </div>
            <!--webscope-->
        </div>
        <!--row-->

    </div>
    <!--container-->
</div>
<!--footer-->

</body>

</html>
