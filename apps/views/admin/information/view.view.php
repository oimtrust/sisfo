<div class="container">
	<div class="card">
	  <div class="card-header">
	    <?php echo $data->title; ?>
	  </div>
	  <div class="card-body">
	    <h5 class="card-title">Dibuat pada tanggal <span class="text-info"><?php echo $data->created_at; ?></span> dan diubah pada tanggal <span class="text-info"><?php echo $data->updated_at; ?></span></h5>
	    <p class="card-text">
	    	<?php echo $data->content; ?>
	    </p>
	    <a href="<?php $baseUrl;?>index.php?page=admin&action=information" class="btn btn-primary">Kembali</a>
	  </div>
	</div>
</div>