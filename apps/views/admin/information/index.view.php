<div class="container">
    <div class="card">
        <div class="card-body">
            Data Praktikum
        </div>
        <div class="card-body">
            <a href="<?php $baseUrl;?>index.php?page=admin&action=information-create" class="btn btn-outline-primary">Tambah Informasi</a>
        </div>
    </div>

    <div class="card">
        <?php
        if (isset($_GET['error'])){
            ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Peringatan!</strong> Anda harus memilih data untuk di ubah.
            </div>
            <?php
        }
        elseif (isset($_GET['saved'])){
            ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Selamat!</strong> Perubahan data berhasil.
            </div>
            <?php
        }
        elseif (isset($_GET['deleted'])){
            ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Selamat!</strong> Data berhasil dihapus.
            </div>
            <?php
        }
        ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Tanggal di posting</th>
                    <th>Tanggal di ubah</th>
                    <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    $limit = 5;
                    $pagination = isset($_GET['pagination']) ? $_GET['pagination'] : "";

                    if (empty($pagination)){
                        $position   = 0;
                        $pagination = 1;
                    }
                    else{
                        $position   = ($pagination - 1) * $limit;
                    }

                    $query = $admins->execute("SELECT id_information, title, created_at, updated_at FROM tbl_information ORDER BY updated_at DESC LIMIT $position, $limit");
                    $no = 1 + $position;

                    while ($data = $query->fetch_assoc()){
                      ?>
                        <tr class="<?php if($no % 2 == 0) {echo "odd";} else {echo "even";} ?>">
                            <th scope="row"><?php echo $no;?></th>
                            <td><?php echo $data['title'];?></td>
                            <td><?php echo $data['created_at'];?></td>
                            <td><?php echo $data['updated_at'];?></td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="<?php $baseUrl;?>index.php?page=admin&action=information-view&view_id=<?php echo $data['id_information'];?>" class="btn btn-secondary btn-warning">Lihat</a>
                                    <a href="<?php $baseUrl;?>index.php?page=admin&action=information-edit&edit_id=<?php echo $data['id_information'];?>" class="btn btn-secondary btn-info">Ubah</a>
                                    <a href="<?php $baseUrl;?>index.php?page=admin&action=information-delete&delete_id=<?php echo $data['id_information'];?>" class="btn btn-secondary btn-danger btn-delete">Hapus</a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <nav >
                <?php
                $amount_data = $admins->execute("SELECT * FROM tbl_information");
                $rows        = $amount_data->num_rows;

                //calculate page
                $amount_page = ceil($rows / $limit);

                /**
                 * navigate previous page
                 */
                if ($pagination > 1){
                    $link = $pagination - 1;
                    $prev = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=information&pagination=$link'>Previous</a>
                            ";
                }
                else {
                    $prev = "<a class='page-link' href='#'>Previous</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }

                /**
                 * navigation after page/next
                 */
                if ($pagination < $amount_page){
                    $link = $pagination + 1;
                    $next = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=information&pagination=$link'>Next</a>
                            ";
                }
                else{
                    $next = "<a class='page-link' href='#'>Next</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }
                echo "<ul class='pagination justify-content-end'>
                <li class='page-item'>
                    ".$prev."
                </li>
                <li class='page-item'>
                    ".$next."
                </li>
            </ul>"
                ?>
        </nav>
    </div>
</div>


<!-- Script JS -->
<script type="text/javascript">

    $('.btn-delete').on('click',function(){
        var getLink = $(this).attr('href');

        swal({
            title: 'Hapus Pengumuman',
            text: 'Anda Yakin?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        },function(){
            window.location.href = getLink
        });

        return false;
    });
</script>