<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                   <fieldset>
                       <?php
                       if (isset($error)){
                           foreach ($error as $error){
                               ?>
                               <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </button>
                                   <strong>Peringatan!</strong> <?php echo $error;?>
                               </div>
                               <?php
                           }
                       }elseif (isset($_GET['saved'])){
                           ?>
                           <div class="alert alert-success alert-dismissible fade show" role="alert">
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                               <strong>Selamat!</strong> Penyimpanan data berhasil. Lihat <a href="<?php $baseUrl;?>index.php?page=admin&action=information">disini</a>
                           </div>
                           <?php
                       }
                       ?>

                       <form method="post">
                           <fieldset >
                              <input type="hidden" name="id_information" value="<?php echo $id_information; ?>">
                               <div class="form-group">
                                   <label for="title">Judul Pengumuman</label>
                                   <input type="text" id="title" name="title" class="form-control" value="<?php echo $data->title; ?>">
                               </div>

                               <div class="form-group">
                                 <label for="content">Konten Pengumuman</label>
                                 <textarea id="content" name="content"><?php echo $data->content; ?></textarea>
                               </div>
                               <button type="submit" name="btn_update_information" class="btn btn-primary">Ubah</button>
                               <a href="javascript:history.back()" class="btn btn-warning">Batal</a>
                           </fieldset>
                       </form>
                   </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  CKEDITOR.replace( 'content' );
</script>