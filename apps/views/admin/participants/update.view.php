<!-- Begin page content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                   <fieldset>
                       <?php
                       if (isset($error)){
                           foreach ($error as $error){
                               ?>
                               <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </button>
                                   <strong>Peringatan!</strong> <?php echo $error;?>
                               </div>
                               <?php
                           }
                       }
                       ?>
                       <form method="post" class="col-md-4 col-sm-12">
                           <fieldset >
                               <div class="form-group">
                                   <label for="npm">NPM</label>
                                   <input type="text" id="npm" name="npm" readonly="readonly" value="<?php echo $data->npm; ?>" class="form-control" >
                               </div>

                               <div class="form-group">
                                   <label for="password">Password</label>
                                   <input type="password" name="password" value="<?php echo $data->password; ?>" class="form-control" >
                               </div>

                               <div class="form-group">
                                   <label for="nama_mhs">Nama Mahasiswa</label>
                                   <input type="text" name="nama_mhs" value="<?php echo $data->nama_mhs; ?>" class="form-control" >
                               </div>

                               <div class="form-group">
                                   <?php
                                   $prodi       = $connect->execute("SELECT * FROM tbl_prodi WHERE id_prodi ORDER BY id_prodi ASC");
                                   $row_count   = $prodi->num_rows;
                                   ?>
                                   <label for="id_prodi">Progam Studi</label>
                                   <select id="id_prodi" name="id_prodi" class="form-control">
                                       <option value="<?php echo $data->id_prodi; ?>" selected><?php echo $data->nama_prodi; ?></option>
                                       <?php
                                       if ($row_count > 0){
                                           while ($row = $prodi->fetch_object()){
                                               echo '<option value="'.$row->id_prodi.'">'.$row->nama_prodi.'</option>';
                                           }
                                       }
                                       else{
                                           echo '<option value="">Prodi tidak tersedia</option>';
                                       }
                                       ?>
                                   </select>
                               </div>

                               <button type="submit" name="btn_update" class="btn btn-primary">Ubah</button>

                               <a href="javascript:history.back()" class="btn btn-warning">Batal</a>
                           </fieldset>
                       </form>
                   </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>