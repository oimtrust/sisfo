<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 9:08
 */?>
<style type="text/css">
    #chart-container {
        width: auto;
        height: auto;
    }
</style>

<div class="container">
    <div id="chart-container">
        <canvas id="evaluation_id"></canvas>
    </div>

    <div class="card">
        <div class="card-body">
            <h3 class="card-title">Keterangan:</h3>
            <?php 
            while ($row_detail = $evaluation_detail->fetch_object()) {
                ?>
                <p>
                    P : Performance = <?php echo $row_detail->q1." %"; ?><br/>
                    I : Information = <?php echo $row_detail->q2." %"; ?><br/>
                    E : Economi = <?php echo $row_detail->q3." %"; ?><br/>
                    C : Control = <?php echo $row_detail->q4." %"; ?><br/>
                    E : Efisien = <?php echo $row_detail->q5." %"; ?><br/>
                    S : Service = <?php echo $row_detail->q6." %"; ?><br/>
                </p>
                <?php
            }
             ?>
        </div>
    </div>
</div>

<script type="text/javascript">

var ctx = document.getElementById("evaluation_id");
var evaluation_id = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['P%', 'I%', 'E%', 'C%', 'E%', 'S%'],
        datasets: [{
            label: '<?php echo $echo_user_ev->total;?> Mahasiswa',
            data: [<?php while ($data = $evaluation->fetch_object()) { 
                echo '"' . $data->q1 . '", "'.$data->q2.'", "'.$data->q3.'", "'.$data->q4.'","'.$data->q5.'", "'.$data->q6.'"';}?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
