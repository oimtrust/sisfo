<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 15:39
 */?>
<div class="container">
    <form method="post">
        <div class="card">
            <div class="card-body">
                Data mahasiswa yang mengikuti praktikum
            </div>
            <div class="card-body">
                <div class="row col-md-12">
                    <a href="<?php $baseUrl;?>index.php?page=admin&action=practicer-export" class="btn btn-outline-success"><i class="fa fa-file-excel-o"></i> Export Data</a>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="table-responsive">
                <table class="table  table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>NPM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Mata Praktikum</th>
                        <th>Semester</th>
                        <th>Progam Studi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $limit = 10;

                    $pagination = isset($_GET['pagination']) ? $_GET['pagination'] : "";

                    if (empty($pagination)){
                        $position   = 0;
                        $pagination = 1;
                    }
                    else{
                        $position   = ($pagination - 1) * $limit;
                    }

                    $query = $admins->execute("SELECT daftar.npm,  GROUP_CONCAT(maprak.mata_praktikum SEPARATOR ', ') as maprak, maprak.semester, mhs.nama_mhs, prodi.nama_prodi
                         FROM
                        tbl_pendaftar AS daftar
                         LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
                         LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
                         LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
                        GROUP BY daftar.npm LIMIT $position, $limit");

                    $no = 1 + $position;
                    while ($data = $query->fetch_object()){
                        ?>
                            <tr class="<?php if($no % 2 == 0) {echo "odd";} else {echo "even";} ?>"$li>
                                <th scope="row"><?php echo $no;?></th>
                                <td><?php echo $data->npm;?></td>
                                <td><?php echo $data->nama_mhs;?></td>
                                <td><?php echo $data->maprak;?></td>
                                <td><?php echo $data->semester;?></td>
                                <td><?php echo $data->nama_prodi;?></td>
                            </tr>
                            <?php
                            $no++;
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <nav>
                <?php
                $amount_data = $admins->execute("SELECT daftar.npm,  GROUP_CONCAT(maprak.mata_praktikum SEPARATOR ', ') as maprak, maprak.semester, mhs.nama_mhs, prodi.nama_prodi
                     FROM
                    tbl_pendaftar AS daftar
                     LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
                     LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
                     LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
                    GROUP BY daftar.npm DESC");
                $rows = $amount_data->num_rows;

                $amount_page = ceil($rows / $limit);

                /**
                 * navigate previous page
                 */
                if ($pagination > 1){
                    $link = $pagination - 1;
                    $prev = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=practicer&pagination=$link'>Previous</a>
                                ";
                }
                else {
                    $prev = "<a class='page-link' href='#'>Previous</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }

                /**
                 * navigation after page/next
                 */
                if ($pagination < $amount_page){
                    $link = $pagination + 1;
                    $next = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=practicer&pagination=$link'>Next</a>
                                ";
                }
                else{
                    $next = "<a class='page-link' href='#'>Next</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }
                echo "<ul class='pagination justify-content-end'>
                    <li class='page-item'>
                        ".$prev."
                    </li>
                    <li class='page-item'>
                        ".$next."
                    </li>
                </ul>"
                ?>
            </nav>
        </div>
    </form>
</div>
