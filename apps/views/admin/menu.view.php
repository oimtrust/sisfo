<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 13:56
 */?>
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #e3f2fd;">
    <div class="container">
    <a class="navbar-brand" href="<?php $baseUrl;?>index.php?page=admin&action=dashboard">SISFO</a>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav nav justify-content-end">
            <li class="nav-item active">
                <a class="nav-link" href="<?php $baseUrl;?>index.php?page=admin&action=dashboard">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMahasiswa" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Mahasiswa
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMahasiswa">
                    <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=admin&action=participants">Data Mahasiswa</a>
                    <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=admin&action=practicer">Data Praktikan</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php $baseUrl;?>index.php?page=admin&action=schedule">Mata Praktikum</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php $baseUrl;?>index.php?page=admin&action=evaluation">Evaluasi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php $baseUrl;?>index.php?page=admin&action=information">Pengumuman</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $admin_login;?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=admin&action=profile">Data Admin</a>
                    <a class="dropdown-item" href="<?php $baseUrl;?>index.php?page=admin&action=logout">Keluar</a>
                </div>
            </li>
        </ul>
    </div>
    </div>
</nav>