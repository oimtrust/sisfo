<!-- Begin page content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                   <fieldset>
                       <?php
                       if (isset($error)){
                           foreach ($error as $error){
                               ?>
                               <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </button>
                                   <strong>Peringatan!</strong> <?php echo $error;?>
                               </div>
                               <?php
                           }
                       }
                       ?>
                       <form method="post" class="col-md-4 col-sm-12">
                           <fieldset >
                              <input type="hidden" name="id_admin" value="<?php echo $data->id_admin; ?>">
                               <div class="form-group">
                                   <label for="username">Username</label>
                                   <input type="text" name="username" class="form-control" value="<?php echo $data->username; ?>" autofocus="autofocus">
                               </div>

                               <div class="form-group">
                                   <label for="password">Password</label>
                                   <input type="password" name="password" value="<?php echo $data->password; ?>" class="form-control" >
                               </div>

                               <button type="submit" name="btn_update" class="btn btn-primary">Ubah</button>

                               <a href="javascript:history.back()" class="btn btn-warning">Batal</a>
                           </fieldset>
                       </form>
                   </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>