<div class="container">
    <form method="post">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <a href="<?php $baseUrl;?>index.php?page=admin&action=profile-create" class="btn btn-outline-primary">Tambah Admin</a>
                    
                    <div class="col-md-6">
                        <div class="input-group">
                          <input type="text" name="search" class="form-control" placeholder="Cari Username..." aria-label="Cari Username...">
                          <span class="input-group-btn">
                            <button class="btn btn-outline-warning" name="btn_search" type="submit">Cari!</button>
                          </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if (isset($_GET['error'])){
           ?>
           <div class="alert alert-danger alert-dismissible fade show" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               <strong>Peringatan!</strong> Anda harus memillih data dahulu untuk diubah
           </div>
           <?php
       }
       elseif (isset($_GET['updated'])){
           ?>
           <div class="alert alert-success alert-dismissible fade show" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               <strong>Selamat!</strong> Perubahan data berhasil.
           </div>
           <?php
       }
       elseif (isset($_GET['deleted'])){
           ?>
           <div class="alert alert-warning alert-dismissible fade show" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               <strong>Selamat!</strong> Data berhasil dihapus.
           </div>
           <?php
       }
       ?>
        <div class="card">
            <div class="table-responsive">
                <table class="table  table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $limit = 10;

                    $pagination = isset($_GET['pagination']) ? $_GET['pagination'] : "";

                    if (empty($pagination)){
                        $position   = 0;
                        $pagination = 1;
                    }
                    else{
                        $position   = ($pagination - 1) * $limit;
                    }

                    $query = $admins->execute("SELECT * FROM tbl_admin ORDER BY updated_at DESC LIMIT $position, $limit");

                    $no = 1 + $position;

                    if (isset($_POST['btn_search'])) {
                        $username = $_POST['search'];

                        if ($username == '') {
                            ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                               <strong>Peringatan!</strong> Kolom pencarian harus di inputkan dahulu
                            </div>
                            <?php
                        }
                        else {
                            if ($username != '') {
                                $query = $connect->execute("SELECT * FROM tbl_admin WHERE username LIKE '%$username%' ORDER BY updated_at DESC LIMIT $position, $limit");
                            }
                            else {
                                $query = $connect->execute("SELECT * FROM tbl_admin ORDER BY updated_at DESC LIMIT $position, $limit");
                            }
                        }
                    }
                    else {
                        $query = $connect->execute("SELECT * FROM tbl_admin ORDER BY updated_at DESC LIMIT $position, $limit");
                    }
                    $check_search = $query->num_rows;

                    if ($check_search < 1) {
                        ?>
                        <tr>
                            <td colspan="3">
                                Data Tidak Ditemukan
                            </td>
                        </tr>
                        <?php
                    }
                    else {
                        while ($data = $query->fetch_object()){
                            ?>
                            <tr class="<?php if($no % 2 == 0) {echo "odd";} else {echo "even";} ?>"$li>
                                <th scope="row"><?php echo $no;?></th>
                                <td><?php echo $data->username;?></td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <a href="<?php $baseUrl;?>index.php?page=admin&action=profile-update&edit_id=<?php echo $data->id_admin; ?>" class="btn btn-outline-info">Ubah</a>
                                       <?php 
                                        if ($data->username != 'admin') {
                                          ?>
                                         <a href="<?php $baseUrl;?>index.php?page=admin&action=profile-delete&delete_id=<?php echo $data->id_admin; ?>" class="btn btn-outline-danger btn-delete">Hapus</a>
                                          <?php
                                        }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $no++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <nav>
                <?php
                $amount_data = $admins->execute("SELECT * FROM tbl_admin ORDER BY updated_at DESC");
                $rows = $amount_data->num_rows;

                $amount_page = ceil($rows / $limit);

                /**
                 * navigate previous page
                 */
                if ($pagination > 1){
                    $link = $pagination - 1;
                    $prev = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=profile&pagination=$link'>Previous</a>
                                ";
                }
                else {
                    $prev = "<a class='page-link' href='#'>Previous</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }

                /**
                 * navigation after page/next
                 */
                if ($pagination < $amount_page){
                    $link = $pagination + 1;
                    $next = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=profile&pagination=$link'>Next</a>
                                ";
                }
                else{
                    $next = "<a class='page-link' href='#'>Next</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }
                echo "<ul class='pagination justify-content-end'>
                    <li class='page-item'>
                        ".$prev."
                    </li>
                    <li class='page-item'>
                        ".$next."
                    </li>
                </ul>"
                ?>
            </nav>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('.btn-delete').on('click',function(){
        var getLink = $(this).attr('href');

        swal({
            title: 'Hapus Admin',
            text: 'Apakah anda yakin?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        },function(){
            window.location.href = getLink
        });

        return false;
    });
</script>