<!-- Begin page content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                   <fieldset>
                       <?php
                       if (isset($error)){
                           foreach ($error as $error){
                               ?>
                               <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </button>
                                   <strong>Peringatan!</strong> <?php echo $error;?>
                               </div>
                               <?php
                           }
                       }elseif (isset($_GET['saved'])){
                           ?>
                           <div class="alert alert-success alert-dismissible fade show" role="alert">
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                               <strong>Selamat!</strong> Penyimpanan data berhasil. Lihat <a href="<?php $baseUrl;?>index.php?page=admin&action=profile">disini</a>
                           </div>
                           <?php
                       }
                       ?>
                       <form method="post" class="col-md-4 col-sm-12">
                           <fieldset >
                               <div class="form-group">
                                   <label for="username">Username</label>
                                   <input type="text" name="username" class="form-control" autofocus="autofocus">
                               </div>

                               <div class="form-group">
                                   <label for="password">Password</label>
                                   <input type="password" name="password" class="form-control" >
                               </div>

                               <button type="submit" name="btn_save" class="btn btn-primary">Tambah</button>

                               <a href="javascript:history.back()" class="btn btn-warning">Batal</a>
                           </fieldset>
                       </form>
                   </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>