<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 15:39
 */?>
<div class="container">
    <div class="card">
        <div class="card-body">
            Data Praktikum
        </div>
        <div class="card-body">
            <a href="<?php $baseUrl;?>index.php?page=admin&action=schedule-create" class="btn btn-outline-primary">Tambah Data</a>
        </div>
    </div>

    <div class="card">
        <?php
        if (isset($_GET['error'])){
            ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Peringatan!</strong> Anda harus memilih data untuk di ubah.
            </div>
            <?php
        }
        elseif (isset($_GET['saved'])){
            ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Selamat!</strong> Perubahan data berhasil.
            </div>
            <?php
        }
        elseif (isset($_GET['deleted'])){
            ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Selamat!</strong> Data berhasil dihapus.
            </div>
            <?php
        }
        ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Mata Praktikum</th>
                    <th>Semester</th>
                    <th>Progam Studi</th>
                    <th>Harga</th>
                    <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    $limit = 5;
                    $pagination = isset($_GET['pagination']) ? $_GET['pagination'] : "";

                    if (empty($pagination)){
                        $position   = 0;
                        $pagination = 1;
                    }
                    else{
                        $position   = ($pagination - 1) * $limit;
                    }

                    $query = $admins->execute("SELECT mapra.id_maprak, mapra.mata_praktikum, mapra.semester, prodi.nama_prodi, mapra.harga FROM tbl_maprak AS mapra LEFT JOIN tbl_prodi AS prodi ON mapra.id_prodi = prodi.id_prodi LIMIT $position, $limit");
                    $no = 1 + $position;

                    while ($data = $query->fetch_assoc()){
                      ?>
                        <tr class="<?php if($no % 2 == 0) {echo "odd";} else {echo "even";} ?>">
                            <th scope="row"><?php echo $no;?></th>
                            <td><?php echo $data['mata_praktikum'];?></td>
                            <td><?php echo $data['semester'];?></td>
                            <td><?php echo $data['nama_prodi'];?></td>
                            <td><?php echo $data['harga'];?></td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="<?php $baseUrl;?>index.php?page=admin&action=schedule-update&edit_id=<?php echo $data['id_maprak'];?>" class="btn btn-secondary btn-info">Ubah</a>
                                    <a href="<?php $baseUrl;?>index.php?page=admin&action=schedule-delete&delete_id=<?php echo $data['id_maprak'];?>" class="btn btn-secondary btn-danger btn-delete">Hapus</a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <nav >
                <?php
                $amount_data = $admins->execute("SELECT * FROM tbl_maprak");
                $rows        = $amount_data->num_rows;

                //calculate page
                $amount_page = ceil($rows / $limit);

                /**
                 * navigate previous page
                 */
                if ($pagination > 1){
                    $link = $pagination - 1;
                    $prev = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=schedule&pagination=$link'>Previous</a>
                            ";
                }
                else {
                    $prev = "<a class='page-link' href='#'>Previous</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }

                /**
                 * navigate number
                 */
//                $page_number = '';
//                for ($i = 1; $i <= $amount_page; $i++){
//                    if ($i == $pagination){
//                        $page_number .= $i."<a class='page-link'></a>";
//                    }
//                    else{
//                        $page_number .= "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=schedule&pagination=$i'>".$i."</a>";
//                    }
//                }

                /**
                 * navigation after page/next
                 */
                if ($pagination < $amount_page){
                    $link = $pagination + 1;
                    $next = "<a class='page-link' href='".$baseUrl."index.php?page=admin&action=schedule&pagination=$link'>Next</a>
                            ";
                }
                else{
                    $next = "<a class='page-link' href='#'>Next</a>"; //".$baseUrl."index.php?page=admin&action=schedule&pagination=$link
                }
                echo "<ul class='pagination justify-content-end'>
                <li class='page-item'>
                    ".$prev."
                </li>
                <li class='page-item'>
                    ".$next."
                </li>
            </ul>"
                ?>
        </nav>
    </div>
</div>


<!-- Script JS -->
<script type="text/javascript">

    $('.btn-delete').on('click',function(){
        var getLink = $(this).attr('href');

        swal({
            title: 'Hapus Mata Praktikum',
            text: 'Anda Yakin?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        },function(){
            window.location.href = getLink
        });

        return false;
    });
</script>