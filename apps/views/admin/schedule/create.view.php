<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 23:50
 */?>
<!-- Begin page content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                   <fieldset>
                       <?php
                       if (isset($error)){
                           foreach ($error as $error){
                               ?>
                               <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </button>
                                   <strong>Peringatan!</strong> <?php echo $error;?>
                               </div>
                               <?php
                           }
                       }elseif (isset($_GET['saved'])){
                           ?>
                           <div class="alert alert-success alert-dismissible fade show" role="alert">
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                               <strong>Selamat!</strong> Penyimpanan data berhasil. Lihat <a href="<?php $baseUrl;?>index.php?page=admin&action=schedule">disini</a>
                           </div>
                           <?php
                       }
                       ?>
                       <form method="post">
                           <fieldset >
                               <div class="form-group">
                                   <label for="mapra">Nama Mata Praktikum</label>
                                   <input type="text" id="mapra" name="maprak" class="form-control" >
                               </div>
                               <div class="form-group">
                                   <label for="semester">Semester</label>
                                   <select id="semester" name="semester" class="form-control">
                                       <option value="" selected>Pilih Semester</option>
                                       <option value="Genap">Genap</option>
                                       <option value="Ganjil">Ganjil</option>
                                   </select>
                               </div>
                               <div class="form-group">
                                   <?php
                                   $prodi       = $connect->execute("SELECT * FROM tbl_prodi WHERE id_prodi ORDER BY id_prodi ASC");
                                   $row_count   = $prodi->num_rows;
                                   ?>
                                   <label for="prodi">Progam Studi</label>
                                   <select id="prodi" name="prodi" class="form-control">
                                       <option value="" selected>Pilih Prodi</option>
                                       <?php
                                       if ($row_count > 0){
                                           while ($row = $prodi->fetch_object()){
                                               echo '<option value="'.$row->id_prodi.'">'.$row->nama_prodi.'</option>';
                                           }
                                       }
                                       else{
                                           echo '<option value="">Prodi tidak tersedia</option>';
                                       }
                                       ?>
                                   </select>
                               </div>
                               <div class="form-group">
                                   <label for="price">Harga</label>
                                   <div class="input-group">
                                       <span class="input-group-addon" id="price">Rp.</span>
                                       <input type="text" name="harga" class="form-control only_number" placeholder="Inputkan hanya angka saja, tanpa titik, koma dan karakter lain" aria-describedby="price">
                                   </div>
                               </div>
                               <button type="submit" name="btn_save_schedule" class="btn btn-primary">Tambah</button>
                               <a href="javascript:history.back()" class="btn btn-warning">Batal</a>
                           </fieldset>
                       </form>
                   </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    (function($){
        $(function(){

            $(".only_number").keypress(function (data)
            {
                // if not number, showing error message
                if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57))
                {
//                    $("#message").html("Inputan tidak valid").show().fadeOut("slow");
                    return false;
                }
            });

        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>