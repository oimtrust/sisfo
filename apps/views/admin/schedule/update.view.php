<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 23:50
 */?>
<!-- Begin page content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-body">
                    <fieldset>
                        <?php
                        if (isset($error)){
                            foreach ($error as $error){
                                ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Peringatan!</strong> <?php echo $error;?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <form method="post">
                            <fieldset >
                                <input type="hidden" name="id_maprak" value="<?php echo $data->id_maprak;?>">
                                <div class="form-group">
                                    <label for="mapra">Nama Mata Praktikum</label>
                                    <input type="text" id="mapra" class="form-control" name="maprak" value="<?php echo $data->mata_praktikum;?>">
                                </div>
                                <div class="form-group">
                                    <label for="semester">Semester</label>
                                    <select id="semester" name="semester" class="form-control">
                                        <option value="" selected>Pilih Semester</option>
                                        <option value="Genap">Genap</option>
                                        <option value="Ganjil">Ganjil</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <?php
                                    $prodi       = $connect->execute("SELECT * FROM tbl_prodi WHERE id_prodi ORDER BY id_prodi ASC");
                                    $row_count   = $prodi->num_rows;
                                    ?>
                                    <label for="prodi">Progam Studi</label>
                                    <select id="prodi" name="prodi" class="form-control">
                                        <option value="" selected>Pilih Prodi</option>
                                        <?php
                                        if ($row_count > 0){
                                            while ($row = $prodi->fetch_object()){
                                                echo '<option value="'.$row->id_prodi.'">'.$row->nama_prodi.'</option>';
                                            }
                                        }
                                        else{
                                            echo '<option value="">Prodi tidak tersedia</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="price">Harga</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="price">Rp.</span>
                                        <input type="text" name="harga" value="<?php echo $data->harga;?>" class="form-control"  aria-describedby="price">
                                    </div>
                                </div>
                                <button type="submit" name="btn_update_schedule" class="btn btn-info">Ubah</button>
                                <a href="javascript:history.back()" class="btn btn-warning">Batal</a>
                            </fieldset>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>