<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 23/08/17
 * Time: 20:42
 */

include_once 'class.connection.php';

class Admin extends Connection
{
    public function schedule_create($maprak, $semester, $prodi, $harga)
    {
        $result = $this->db->query(
            "INSERT INTO tbl_maprak (mata_praktikum, semester, id_prodi, harga)
                  VALUES ('{$maprak}', '{$semester}', '{$prodi}', '{$harga}')"
        );
    }

    public function schedule_update($maprak, $semester, $prodi, $harga, $id)
    {
        $result = $this->db->query(
            "UPDATE tbl_maprak SET mata_praktikum='{$maprak}', semester='{$semester}',
            id_prodi='{$prodi}', harga='{$harga}' WHERE id_maprak='{$id}'"
        );
    }

    public function schedule_delete($id)
    {
        $result = $this->db->query("DELETE FROM tbl_maprak WHERE id_maprak='{$id}'");
    }

    public function information_create($title, $content, $created_at, $updated_at)
    {
        $result = $this->db->query("INSERT INTO tbl_information (title, content, created_at, updated_at) VALUES ('{$title}', '{$content}', '{$created_at}', '{$updated_at}')");
    }

    public function information_update($title, $content, $updated_at, $id_information)
    {
        $result = $this->db->query("UPDATE tbl_information SET title = '{$title}', content = '{$content}', updated_at = '{$updated_at}' WHERE id_information = '{$id_information}'");
    }

    public function information_delete($id_information)
    {
        $result = $this->db->query("DELETE FROM tbl_information WHERE id_information='{$id_information}'");
    }

    public function createParticipants($npm, $password, $nama_mhs, $id_prodi, $created_at, $updated_at)
    {
        $result = $this->db->query("INSERT INTO tbl_mahasiswa(npm, password, nama_mhs, id_prodi, created_at, updated_at) VALUES('{$npm}', '{$password}', '{$nama_mhs}', '{$id_prodi}', '$created_at', '{$updated_at}')");
    }

    public function updateParticipants($password, $nama_mhs, $id_prodi, $updated_at, $npm)
    {
        $result = $this->db->query("UPDATE tbl_mahasiswa SET password = '{$password}', nama_mhs = '{$nama_mhs}', id_prodi = '{$id_prodi}', updated_at = '{$updated_at}' WHERE npm = '{$npm}'");
    }

    public function deleteParticipants($npm)
    {
        $result = $this->db->query("DELETE FROM tbl_mahasiswa WHERE npm='{$npm}'");
    }

    public function createProfile($username, $password, $created_at, $updated_at)
    {
        $result = $this->db->query("INSERT INTO tbl_admin(username, password, created_at, updated_at)
            VALUES('{$username}', '{$password}', '{$created_at}', '{$updated_at}')");
    }

    public function updateProfile($username, $password, $updated_at, $id_admin)
    {
        $result = $this->db->query("UPDATE tbl_admin SET username = '{$username}', password = '{$password}', updated_at = '{$updated_at}' WHERE id_admin = '{$id_admin}'");
    }

    public function deleteProfile($id_admin)
    {
        $result = $this->db->query("DELETE FROM tbl_admin WHERE id_admin = '{$id_admin}'");
    }
}