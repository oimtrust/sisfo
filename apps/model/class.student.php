<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 24/08/17
 * Time: 10:59
 */
include_once 'class.connection.php';

class Student extends Connection
{
    public function practicum_create($npm, $imaprak, $total)
    {
        $result = $this->db->query("INSERT INTO tbl_pendaftar (npm, id_maprak, total) 
        VALUES ('{$npm}', '{$imaprak}', '{$total}')");
    }

    public function practicum_update($npm, $imaprak, $total, $id)
    {
        $result = $this->db->query("UPDATE tbl_pendaftar SET npm='{$npm}', id_maprak='{$imaprak}',
                  total='{$total}' WHERE id_pendaftar='{$id}'");
    }

    public function practicum_delete($id)
    {
        $result = $this->db->query("DELETE FROM tbl_pendaftar WHERE id_pendaftar='{$id}'");
    }

    public function send_evaluation($q1, $q2, $q3, $q4, $q5, $q6, $npm)
    {
        $result = $this->db->query(
            "INSERT INTO tbl_evaluasi(q1, q2, q3, q4, q5, q6, npm)
                  VALUES ('{$q1}', '{$q2}', '{$q3}', '{$q4}', '{$q5}', '{$q6}', '{$npm}')");
    }

    public function update_password($npm, $password)
    {
        $result = $this->db->query("UPDATE tbl_mahasiswa SET password='{$password}' WHERE npm='{$npm}'");
    }
}