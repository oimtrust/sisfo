SELECT SUM(harga_mk) AS jumlah FROM tbl_pendaftar GROUP BY npm='166403020002';

SELECT mapra.mata_praktikum, mapra.semester, prodi.nama_prodi, mapra.harga FROM tbl_maprak AS mapra JOIN tbl_prodi AS prodi ON mapra.id_maprak = prodi.id_prodi;

# three table relation
SELECT
  daftar.id_pendaftar,
  daftar.npm,
  daftar.total,
  maprak.mata_praktikum,
  maprak.semester
FROM
  tbl_pendaftar AS daftar
  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
WHERE daftar.npm='166403020002';

#for bill
SELECT
  daftar.id_pendaftar,
  daftar.npm,
  daftar.total,
  maprak.mata_praktikum,
  maprak.semester,
  mhs.nama_mhs,
  prodi.nama_prodi
FROM
  tbl_pendaftar AS daftar
  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
  LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
  LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
WHERE daftar.npm='166403020002';

#for participants
SELECT
  mhs.npm,
  mhs.nama_mhs,
  prodi.nama_prodi
FROM
  tbl_mahasiswa AS mhs
  LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
WHERE mhs.npm;

#for practicer
SELECT
  daftar.npm,
  maprak.mata_praktikum,
  maprak.semester,
  mhs.nama_mhs,
  prodi.nama_prodi
FROM
  tbl_pendaftar AS daftar
  LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
  LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
  LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi

#for tbl_pendaftar group by using concat
SELECT daftar.npm,  GROUP_CONCAT(maprak.mata_praktikum SEPARATOR ', ') as maprak, maprak.semester, mhs.nama_mhs, prodi.nama_prodi
 FROM
tbl_pendaftar AS daftar
 LEFT JOIN tbl_maprak AS maprak ON daftar.id_maprak = maprak.id_maprak
 LEFT JOIN tbl_mahasiswa AS mhs ON daftar.npm = mhs.npm
 LEFT JOIN tbl_prodi AS prodi ON mhs.id_prodi = prodi.id_prodi
GROUP BY daftar.npm