<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 11:38
 */
if(defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}


/**
 * show error 500
 */
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/index.view.php';
include 'apps/views/layouts/footer.view.php';