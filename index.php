<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 22/08/17
 * Time: 11:30
 */
define('RESTRICTED', 1);

//Ensure that a session exists (just in case)
if (!session_id())
{
    session_start();
}


require 'vendor/autoload.php';
require 'apps/config/app.php';
require_once 'apps/model/class.connection.php';
require_once 'apps/model/class.admin.php';
require_once 'apps/model/class.student.php';

use Spipu\Html2Pdf\Html2Pdf;

$html2pdf = new Html2Pdf('P', 'A4','fr', false, 'ISO-8859-15',array(2, 2, 2, 2));

$connect    = new Connection();
$admins     = new Admin();
$students   = new Student();

//here our routes
$page   = (!empty($_GET['page'])) ? $_GET['page'] : null;
$action = (!empty($_GET['action'])) ? $_GET['action'] : null;

switch ($page){
    case 'admin':
        if ($action == 'login'){
            require 'admin/auth/login.php';
        }
        elseif ($action == 'logout'){
            require 'admin/auth/logout.php';
        }
        elseif ($action == 'dashboard'){
            require 'admin/index.php';
        }
        elseif ($action == 'profile') {
            require 'admin/profile.php';
        }
        elseif ($action == 'profile-create') {
            require 'admin/profile-create.php';
        }
        elseif ($action == 'profile-update') {
            require 'admin/profile-update.php';
        }
        elseif ($action == 'profile-delete') {
            require 'admin/profile-delete.php';
        }
        elseif ($action == 'participants'){
            require 'admin/participants.php';
        }
        elseif ($action == 'participants-create') {
            require 'admin/participants-create.php';
        }
        elseif ($action == 'participants-update') {
            require 'admin/participants-update.php';
        }
        elseif ($action == 'participants-delete') {
            require 'admin/participants-delete.php';
        }
        elseif ($action == 'practicer'){
            require 'admin/practicer.php';
        }
        elseif ($action == 'practicer-export'){
            require 'admin/practicer-export.php';
        }
        elseif ($action == 'schedule'){
            require 'admin/schedule.php';
        }
        elseif ($action == 'schedule-create'){
            require 'admin/schedule-create.php';
        }
        elseif ($action == 'schedule-update'){
            require 'admin/schedule-update.php';
        }
        elseif ($action == 'schedule-delete'){
            require 'admin/schedule-delete.php';
        }
        elseif ($action == 'evaluation'){
            require 'admin/evaluation.php';
        }
        elseif ($action == 'information') {
            require 'admin/information.php';
        }
        elseif ($action == 'information-create') {
            require 'admin/information-create.php';
        }
        elseif ($action == 'information-delete') {
            require 'admin/information-delete.php';
        }
        elseif ($action == 'information-edit') {
            require 'admin/information-edit.php';
        }
        elseif ($action == 'information-view') {
            require 'admin/information-view.php';
        }
        else {
            require 'error/404.php';
        }
        break;

    case 'student':
        if ($action == 'login'){
            require 'student/auth/login.php';
        }
        elseif ($action == 'logout'){
            require 'student/auth/logout.php';
        }
        elseif ($action == 'dashboard'){
            require 'student/index.php';
        }
        elseif ($action == 'practicum'){
            require 'student/practicum/index.php';
        }
        elseif ($action == 'practicum-create'){
            require 'student/practicum/create.php';
        }
        elseif ($action == 'practicum-create-dynamix'){
            require 'student/practicum/create-dynamix.php';
        }
        elseif ($action == 'practicum-update'){
            require 'student/practicum/update.php';
        }
        elseif ($action == 'practicum-delete'){
            require 'student/practicum/delete.php';
        }
        elseif ($action == 'evaluation'){
            require 'student/evaluation/index.php';
        }
        elseif ($action == 'bill'){
            require 'student/bill.php';
        }
        elseif ($action == 'bill-printout'){
            require 'student/bill-printout.php';
        }
        elseif ($action == 'profile'){
            require 'student/profile.php';
        }
        elseif ($action == 'profile-update'){
            require 'student/profile-update.php';
        }
        else {
            require 'error/404.php';
        }
        break;

    case 'home':
        if ($action == 'vmision') {
            require 'home/vmision.php';
        }
        elseif($action == 'information'){
            require 'home/information.php';
        } 
        else {
            require 'error/404.php';
        }
        break;
        

    default:
        require 'home.php';
        break;
}